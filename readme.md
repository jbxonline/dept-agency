# DEPT Agency 

## Technical test

I've used Laravel 5.5 for this project as that was the version referenced in most of the documentation links in the email and so I've assumed that is your current version.

I've included a `docker-compose.yml` file that will create a working test environment based on PHP 7.2.9 and MariaDb.

### Installation

Simply `cp .env.example .env` and then `docker-compose up -d` and the project should be available at localhost. You will need to login to the web container (`docker container exec -it dept-agency_web_1 bash`) and run `php artisan migrate` to get your db table.

I've included the compiled assets in the git repo, so you don't have to worry about faffing with npm.

### Vue.js update

I've updated the frontend to use Vue.js. To pretty things up a bit I used Vuetify, although I stuck with standard DataTables rendered with Vue. 