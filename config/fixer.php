<?php return [
    'api_key' => env('FIXER_API_KEY', null),
    'host'    => env('FIXER_HOST', 'http://data.fixer.io/api/')
];
