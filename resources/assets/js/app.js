
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('data-table', require('./components/DataTable.vue'));
import Vuetify from 'vuetify';
Vue.use(Vuetify);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi'
        }
    }),
    data: () => ({
        drawer: null,
        menu: false,
        currencies: ['GBP','USD','CAD','AUD'],
        birthday: new Date().toISOString().substr(0, 10),
        birthdayError: false,
        currency: '',
        currencyError: false,
        columns: [
            {data: 'format_birthday', name: 'birthday', title: 'Birthday'},
            {data: 'base'},
            {data: 'currency'},
            {data: 'rate'},
            {data: 'count'},
        ]
    }),
    methods: {
        processForm: async function() {
            let formData = new FormData()
            const props  = this

            // clear any error states
            props.birthdayError = false
            props.currencyError = false

            formData.append('birthday', this.birthday)
            formData.append('currency', this.currency)

            await axios.post('/rate', formData).then(function (response) {
                toastr.success(response.data.message ? response.data.message : 'Record updated');
            }).catch(function (error) {
                let data = error.response.data
                toastr.error(data.message)

                if (typeof data.errors.birthday !== 'undefined') {
                    props.birthdayError = data.errors.birthday
                }

                if (typeof data.errors.currency !== 'undefined') {
                    props.currencyError = data.errors.currency
                }
            })
        }
    },
});
