<?php return [
    'titles' => [
        'meta_title' => 'Birthday Exchange Rates',
        'home'       => 'Birthday exchange rates.',
        'subtitle' => 'Find out the exchange rate on your birthday',
        'intro' => 'We just need the date of your last birthday and the rate you want to lookup',
    ],

    'messages' => [
        'thanks' => 'Thanks for taking part! Click the menu link to view all results...',
        'unknown_error' => 'Fixer returned an invalid response.'
    ],

    'labels' => [
        'birthday' => 'Select the date of your last birthday',
        'currency' => 'Select the currency to compare against',
        'please_select' => 'Please select'
    ]
];
