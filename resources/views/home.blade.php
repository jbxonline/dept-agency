@extends('layouts.app')

@section('content')

    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible">
            {{ session('error') }}
        </div>
    @endif

    @if (session()->has('success') && session('success', false) === true)
        <div class="alert alert-success alert-dismissible">
            {{ trans('ui.messages.thanks') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <v-row>
        <v-col cols="6">
            <h2 class="my-5 font-weight-normal text-center">{{ trans('ui.titles.home') }}</h2>
            <p class="intro text-center">
                {{ trans('ui.titles.subtitle') }}
                <br>
                {{ trans('ui.titles.intro') }}
            </p>

            <v-form @submit.prevent="processForm">
                {{ csrf_field() }}
                <v-menu
                    v-model="menu"
                    :close-on-content-click="false"
                    :nudge-right="40"
                    transition="scale-transition"
                    offset-y
                    min-width="290px"
                >
                    <template v-slot:activator="{ on }">
                        <v-text-field
                            v-model="birthday"
                            label="{{ trans('ui.labels.birthday') }}"
                            prepend-icon="mdi-calendar-check-outline"
                            readonly
                            v-on="on"
                            :error="birthdayError"
                            :error-messages="birthdayError"
                        ></v-text-field>
                    </template>
                    <v-date-picker v-model="birthday" @input="menu = false"></v-date-picker>
                </v-menu>

                <v-select
                    v-model="currency"
                    :items="currencies"
                    :error="currencyError"
                    :error-messages="currencyError"
                    label="{{ trans('ui.labels.currency') }}"
                >
                    <template v-slot:selection="{ item, index }">
                        <v-chip v-if="index === 0">
                            <span>@{{ item }}</span>
                        </v-chip>
                        <span
                                v-if="index === 1"
                                class="grey--text caption"
                        >(+@{{ currency.length - 1 }} others)</span>
                    </template>
                </v-select>

                <input type="submit" class="btn btn-primary mt-5">
            </v-form>
        </v-col>
    </v-row>

@endsection