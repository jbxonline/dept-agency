@extends('layouts.app')

@section('content')
    <v-row>
        <v-col cols="12">
            <data-table :columns="columns" class="table" ></data-table>
        </v-col>
    </v-row>
@endsection