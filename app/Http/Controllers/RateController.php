<?php

namespace App\Http\Controllers;

use App\DataTables\RateDataTable;
use App\Fixer\Client;
use App\Http\Requests\StoreRate;
use App\Rate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RateDataTable $dt)
    {
        return $dt->render('rates.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRate $request
     * @param  Client $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRate $request, Client $client)
    {
        $validated = $request->validated();

        $response  = $client->getExchangeRate(
            Carbon::createFromFormat('Y-m-d', $validated['birthday']),
            $validated['currency']
        );

        if (!$response) {
            return response()->json(['message' => $client->getError()], 500);
        }

        $rate   = $client->getRate();
        $record = Rate::where('birthday', $validated['birthday'])
            ->where('base', $rate['base'])
            ->first();

        if ($record) {
            $record->increment('count');
        } else {
            Rate::create([
                'birthday' => $validated['birthday'],
                'base'     => $rate['base'],
                'currency' => $rate['currency'],
                'rate'     => $rate['rate']
            ]);
        }

        $request->session()->flash('success', true);
        return response()->json(['message' => trans('ui.messages.thanks')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        //
    }
}
