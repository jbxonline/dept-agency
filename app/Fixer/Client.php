<?php

namespace App\Fixer;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;

class Client
{
    private $client;
    private $apiKey;
    private $message;
    private $body;

    public function __construct()
    {
        $endpoint     = config('fixer.host');
        $this->apiKey = config('fixer.api_key');
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => $endpoint
        ]);
    }

    public function getExchangeRate(Carbon $date, $currency)
    {
        $params = [
            'access_key' => $this->apiKey,
            'symbols' => $currency
        ];

        try {
            $response = $this->client->get($date->format('Y-m-d'), [
                'query' => $params
            ]);
            $this->body = json_decode($response->getBody());
            if (!$this->body) {
                $this->success = false;
                $this->message = trans('ui.messages.unknown_error');
            } else {
                $this->success = (bool) $this->body->success;
            }
        } catch (GuzzleException $e) {
            $this->success = false;
            $this->message = $e->getMessage();
        }

        return $this->success;
    }

    public function getError()
    {
        if ($this->success === true) {
            return false;
        }

        if (!empty($this->message)) {
            return $this->message;
        }

        return trans('fixer_errors.' . (int) $this->body->error->code);
    }

    public function getRate()
    {
        if (!$this->success === true) {
            return false;
        }

        $rates = array();
        foreach ($this->body->rates as $currency => $rate) {
            array_push($rates, [
                'base'     => $this->body->base,
                'currency' => $currency,
                'rate'     => $rate
            ]);
        }

        // In this app, we only ever request a single rate, so we only need to return the first item in the array.
        return $rates[0];
    }
}